namespace Fijo.Code.NanoCode {
	public delegate object NanoCodeFunc(params object[] arguments);
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fijo.Code.NanoCode {
	public class NanoCode {
		private string StrIn(string s, int index, int until) {
			return new string(StrInIterator(s.Skip(index).Take(until - index), '\\').ToArray());
		}

		private IEnumerable<char> StrInIterator(IEnumerable<char> chars, char escapeChar) {
			var skipNext = false;
			foreach (var @char in chars) {
				if(skipNext) skipNext = false;
				else if(@char != escapeChar) yield return @char;
				else skipNext = true;
			}
		}

		public void ExecCode(string[] s) {
			int index = 0;
			foreach (var current in s) {
				var v = (int) ExecFunc(current, index++, true);
				if(v != 0) index = v;
			}
		}

		private readonly IList<bool> _ifs = new List<bool>();
		private int _ifsCount;
		public IDictionary<string, object> Vars = new Dictionary<string, object>();
		private readonly IDictionary<string, int> _namespaces = new Dictionary<string, int>();
		public StringBuilder Response = new StringBuilder();
		private readonly IDictionary<string, NanoCodeFunc> _funcs = new Dictionary<string, NanoCodeFunc>
		{
			{"sum", arguments => (object)((double)arguments[0] + (double)arguments[1])},
			{"diff", arguments => (object)((double)arguments[0] - (double)arguments[1])},
			{"times", arguments => (object)((double)arguments[0] * (double)arguments[1])},
			{"divi", arguments => (object)((double)arguments[0] / (double)arguments[1])},
			{"and", arguments => (object)((bool)arguments[0] && (bool)arguments[1])},
			{"or", arguments => (object)((bool)arguments[0] || (bool)arguments[1])},
		};

		private object ExecFunc(string source, int q, bool isEntryPoint) {
			var l = source.Length;
			var p = source.IndexOf(':');
			var func = source.Substring(0, p++);
			if (isEntryPoint) {
				object gotoTargetFor;
				if (ExecuteFuncFromEntryPoint(source, q, func, p, out gotoTargetFor)) return gotoTargetFor;
			}

			var iFunc = 0;
			var a = new object[255];
			var L = 0;
			var b = p;

			while (p < l)
					switch (source[p++]) {
						case '\\':
							p += 2;
							break;
						case ':':
							iFunc++;
							break;
						case ',':
							if (iFunc == 0) a[L++] = StrIn(source, b, (b = p) - 1);
							break;
						case '.':
							if (iFunc-- == 1) a[L++] = ExecFunc(source.Substring(b, (b = p) - 1), 0, false);
							break;
						case '_':
							if ((iFunc -= int.Parse(source.Substring(p, p + 2))) == 1) {
								a[L++] = ExecFunc(source.Substring(b, (b = p) - 1), 0, false);
								b += 2;
							}
							p += 2;
							break;
					}

			a[L++] = iFunc != 0
			         	? ExecFunc(source.Substring(b, p), 0, false)
			         	: StrIn(source, b, p);

			var valueToReturn = Funcs(func, a);
			return isEntryPoint == false
			       	? valueToReturn
			       	: 0;
		}

		private bool ExecuteFuncFromEntryPoint(string source, int q, string func, int p, out object gotoTargetFor) {
			if (!CurrentExecCondition() &&
			    func == "else" &&
			    func == "elseif" &&
			    func == "butif" &&
			    func == "fi") {
				gotoTargetFor = 0;
				return true;
			}
			switch (func) {
				case "namespace":
					_namespaces.Add(GetFirstParam(source, p), q);
					gotoTargetFor = 0;
					return true;
				case "gotofi":
					_ifsCount--;
					goto case "goto";
				case "goto":
					gotoTargetFor = GetGotoTargetFor(source, p);
					return true;
				case "gotofa":
					_ifsCount = 0;
					gotoTargetFor = GetGotoTargetFor(source, p);
					return true;
			}
			gotoTargetFor = default(object);
			return false;
		}

		private static string GetFirstParam(string source, int p) {
			return source.Substring(p);
		}

		private int GetGotoTargetFor(string source, int p) {
			var target = GetFirstParam(source, p);
			int targetPos;
			if(!_namespaces.TryGetValue(target, out targetPos)) throw new Exception("A namespace '" + source.Substring(p) + "' does not exist");
			return targetPos;
		}

		private bool CurrentExecCondition() {
			return _ifs.Count == 0 || _ifs[_ifsCount];
		}

		private object Funcs(string funcName, params object[] arguments) {
			return _funcs[funcName](arguments);
		}
	}
}